﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardManager : MonoBehaviour
{
    public static BoardManager Instance { set; get; }
    private bool[,] allowedMoves { set; get; }

    public Chessman[,] Chessmans { set; get; }
    private Chessman selectedChessman;

    private const float TILE_SIZE = 1.0f;
    private const float TILE_OFFSET = 0.5f;

    private int selectionX = -1;
    private int selectionY = -1;

    public List<GameObject> chessmanPrefabs;
    private List<GameObject> activeChessman;

    private Quaternion GoldRot = Quaternion.Euler(0, 90, 0);
    private Quaternion SilverRot = Quaternion.Euler(0, -90, 0);

    //private Material previousMat;
    public Material selectedMat;

    public bool isWhiteTurn = true;
    public bool isCastling = false;

    public int[] EnPassantMove { set; get; }
    public int[] CastlingMove { set; get; }

    private bool whiteCastlingFlag = false;
    private bool blackCastlingFlag = false;
    private bool whiteAtLeastOneMove = false;
    private bool blackAtLeastOneMove = false;
    public bool promotionWhiteQueen;
    public bool promotionWhiteKngiht;
    public bool promotionBlackQueen;
    public bool promotionBlackKngiht;

    public GameObject LeftWhiteRook;
    public GameObject RightWhiteRook;
    public GameObject LeftBlackRook;
    public GameObject RightBlackRook;
    public GameObject QueenButton;
    public GameObject KnightButton;

    private void Start()
    {
        isWhiteTurn = true;
        Instance = this;
        SpawnAllChessman();
        QueenButton.SetActive(false);
        KnightButton.SetActive(false);
    }

    //ここにプロモーション条件式書けばいい？
    private void Update()
    {
        UpdateSelection();
        DrawChessboard();
        if (Input.GetMouseButtonDown(0))
        {
            if(selectionX >= 0 && selectionY >= 0)
            {
                if(selectedChessman == null)
                {
                    // Select the chessman
                    SelectChessman(selectionX, selectionY);
                }
                else if (selectionY == 7 && isWhiteTurn && selectedChessman.GetType() == typeof(Pawn))
                {
                    isWhiteTurn = true;
                    QueenButton.SetActive(true);
                    KnightButton.SetActive(true);
                    if(promotionWhiteQueen == true)
                    {
                        Debug.Log("promotionWhiteQueen");
                        MoveChessman(selectionX, selectionY);
                        QueenButton.SetActive(false);
                        KnightButton.SetActive(false);
                    }
                    else if(promotionWhiteKngiht == true)
                    {
                        Debug.Log("promotionWhiteKnight");
                        MoveChessman(selectionX, selectionY);
                        QueenButton.SetActive(false);
                        KnightButton.SetActive(false);
                    }
                }
                else if(selectionY == 0 && !isWhiteTurn && selectedChessman.GetType() == typeof(Pawn))
                {
                    isWhiteTurn = false;
                    QueenButton.SetActive(true);
                    KnightButton.SetActive(true);
                    if (promotionBlackQueen == true)
                    {
                        Debug.Log("promotionBlackQueen");
                        MoveChessman(selectionX, selectionY);
                        QueenButton.SetActive(false);
                        KnightButton.SetActive(false);
                    }
                    else if (promotionBlackKngiht == true)
                    {
                        Debug.Log("promotionBlackKnight");
                        MoveChessman(selectionX, selectionY);
                        QueenButton.SetActive(false);
                        KnightButton.SetActive(false);
                    }
                }
                else
                {
                    // Move the chessman
                    MoveChessman(selectionX, selectionY);
                    Debug.Log("selectionX,  selectionY" + selectionX + selectionY);
                }
            }
        }
    }

    private void SelectChessman(int x, int y)
    {
        if (Chessmans[x, y] == null)
            return;

        if (Chessmans[x, y].isWhite != isWhiteTurn)
            return;

        bool hasAtleastOneMove = false;
        allowedMoves = Chessmans[x, y].PossibleMove();
        for (int i = 0; i < 8; i++)
            for (int j = 0; j < 8; j++)
                if (allowedMoves[i, j])
                    hasAtleastOneMove = true;

        if (!hasAtleastOneMove)
            return;

        selectedChessman = Chessmans[x, y];
        //previousMat = selectedChessman.GetComponent<MeshRenderer>().material;
        //selectedMat.mainTexture = previousMat.mainTexture;
        //selectedChessman.GetComponent<MeshRenderer>().material = selectedMat;
        BoardHighlights.Instance.HighlightsAllowedMoves(allowedMoves);
    }

    private void MoveChessman(int x, int y)
    {
        if(allowedMoves[x,y])
        {
            Chessman c = Chessmans[x, y];

            //敵の駒を取る
            if (c != null && c.isWhite != isWhiteTurn)
            {
                //Capture a place

                //If it is the king
                if(c.GetType() == typeof(King))
                {
                    EndGame();
                    return;
                }

                activeChessman.Remove(c.gameObject);
                Destroy(c.gameObject);
            }

            //アンパサムーブ
            if(x == EnPassantMove[0] && y == EnPassantMove[1])
            {
                //White turn
                if(isWhiteTurn)
                    c = Chessmans[x, y - 1];
                //Black team
                else
                    c = Chessmans[x, y + 1];
                    activeChessman.Remove(c.gameObject);
                    Destroy(c.gameObject);
            }
            EnPassantMove[0] = -1;
            EnPassantMove[1] = -1;
            if (selectedChessman.GetType() == typeof(Pawn))
            {
                //white pawn promotion
                if(y == 7)
                {
                    if (promotionWhiteQueen == true && isWhiteTurn)
                    {
                        activeChessman.Remove(selectedChessman.gameObject);
                        Destroy(selectedChessman.gameObject);
                        SpawnChessman(1, x, y, GoldRot);
                        selectedChessman = Chessmans[x, y];
                        promotionWhiteQueen = false;
                        Debug.Log("Queen");
                    }
                    else if(promotionWhiteKngiht == true && isWhiteTurn)
                    {
                        activeChessman.Remove(selectedChessman.gameObject);
                        Destroy(selectedChessman.gameObject);
                        SpawnChessman(5, x, y, GoldRot);
                        selectedChessman = Chessmans[x, y];
                        promotionWhiteKngiht = false;
                        Debug.Log("Knight");
                    }
                }
                //black pawn promotion
                else if( y == 0)
                {
                    if (promotionBlackQueen == true && !isWhiteTurn)
                    {
                        activeChessman.Remove(selectedChessman.gameObject);
                        Destroy(selectedChessman.gameObject);
                        SpawnChessman(8, x, y, SilverRot);
                        selectedChessman = Chessmans[x, y];
                        promotionBlackQueen = false;
                        Debug.Log("Queen");
                    }
                    else if (promotionBlackKngiht == true && !isWhiteTurn)
                    {
                        activeChessman.Remove(selectedChessman.gameObject);
                        Destroy(selectedChessman.gameObject);
                        SpawnChessman(12, x, y, GoldRot);
                        selectedChessman = Chessmans[x, y];
                        promotionBlackKngiht = false;
                        Debug.Log("Knight");
                    }
                }
                if (selectedChessman.CurrentY == 1 && y == 3)
                {
                    EnPassantMove[0] = x;
                    EnPassantMove[1] = y -1;
                    //Debug.Log(EnPassantMove[0]);
                    //Debug.Log(EnPassantMove[1]);
                }
                else if (selectedChessman.CurrentY == 6 && y == 4)
                {
                    EnPassantMove[0] = x;
                    EnPassantMove[1] = y + 1;
                }
            }


            //WhiteCastling
            if (selectedChessman.GetType() == typeof(King) && isWhiteTurn && c == null && whiteAtLeastOneMove == false)
            {
                whiteAtLeastOneMove = true;
                if (x == 6 && y == 0 && whiteCastlingFlag == false && whiteAtLeastOneMove == true)
                {
                    whiteCastlingFlag = true;
                    Destroy(GameObject.Find(RightWhiteRook.name + "(Clone)"));
                    SpawnChessman(3, x - 1, y, GoldRot);
                }
                else if (Chessmans[1, 0] == null && whiteAtLeastOneMove == true)
                {
                    if(x == 2 && y == 0 && whiteCastlingFlag == false )
                    {
                        whiteCastlingFlag = true;
                        Destroy(GameObject.Find(LeftWhiteRook.name + "(Clone)"));
                        SpawnChessman(2, x + 1, y, GoldRot);
                    }
                }
            }
            //BlackCastling
            else if (selectedChessman.GetType() == typeof(King) && !isWhiteTurn && c == null && blackAtLeastOneMove == false)
            {
                blackAtLeastOneMove = true;
                if (x == 6 && y == 7 && blackCastlingFlag == false && blackAtLeastOneMove == true)
                {
                    blackCastlingFlag = true;
                    Destroy(GameObject.Find(RightBlackRook.name + "(Clone)"));
                    SpawnChessman(9, x - 1, y, SilverRot);
                }
                else if (Chessmans[1, 7] == null && blackAtLeastOneMove == true)
                {
                   if(x == 2 && y == 7 && blackCastlingFlag == false )
                    {
                        blackCastlingFlag = true;
                        Destroy(GameObject.Find(LeftBlackRook.name + "(Clone)"));
                        SpawnChessman(10, x + 1, y, SilverRot);
                    }
                }
            }

            Chessmans[selectedChessman.CurrentX, selectedChessman.CurrentY] = null;
            selectedChessman.transform.position = GetTileCenter(x, y);
            selectedChessman.SetPosition(x, y);
            Chessmans[x, y] = selectedChessman;
            isWhiteTurn = !isWhiteTurn;
        }

        //selectedChessman.GetComponent<MeshRenderer>().material = previousMat;
        BoardHighlights.Instance.Hidehighlights();
        selectedChessman = null;
    }

    private void UpdateSelection()
    {
        if (!Camera.main)
            return;

        RaycastHit hit;
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 25.0f, LayerMask.GetMask("ChessPlane")))
        {
            selectionX = (int)hit.point.x;
            selectionY = (int)hit.point.z;
        }
        else
        {
            selectionX = -1;
            selectionY = -1;
         }
    }

    private void SpawnChessman(int index, int x, int y, Quaternion rot)
    {
        GameObject go = Instantiate(chessmanPrefabs[index], GetTileCenter(x,y), rot) as GameObject;
        go.transform.SetParent(transform);
        Chessmans[x, y] = go.GetComponent<Chessman>();
        Chessmans[x, y].SetPosition(x, y);
        activeChessman.Add(go);
    }

    //private
    public void SpawnAllChessman()
    {
        activeChessman = new List<GameObject>();
        Chessmans = new Chessman[8, 8];
        EnPassantMove = new int[2] { -1, -1 };
        CastlingMove = new int[2] { +2, 0 };

        // Spawn the white team!

        //King
        SpawnChessman(0, 4, 0, GoldRot);

        //Queen
        SpawnChessman(1, 3, 0, GoldRot);

        //Rooks
        //Left
        SpawnChessman(2, 0, 0, GoldRot);
        //Right
        SpawnChessman(3, 7, 0, GoldRot);

        //Bishops
        SpawnChessman(4, 2, 0, GoldRot);
        SpawnChessman(4, 5, 0, GoldRot);

        //Knights
        SpawnChessman(5, 1, 0, GoldRot);
        SpawnChessman(5, 6, 0, GoldRot);

        //Pawns
        for (int i = 0; i < 8; i++)
        SpawnChessman(6, i, 1, GoldRot);


        //Spawn the black team!

        //King
        SpawnChessman(7, 4, 7, SilverRot);

        //Queen
        SpawnChessman(8, 3, 7, SilverRot);

        //Rooks
        //Left
        SpawnChessman(9, 0, 7, SilverRot);
        //Right
        SpawnChessman(10, 7, 7, SilverRot);

        //Bishops
        SpawnChessman(11, 2, 7, SilverRot);
        SpawnChessman(11, 5, 7, SilverRot);

        //Knights
        SpawnChessman(12, 1, 7, SilverRot);
        SpawnChessman(12, 6, 7, SilverRot);

        //Pawns
        for (int i = 0; i < 8; i++)
            SpawnChessman(13, i, 6, SilverRot);
    }

    private Vector3 GetTileCenter(int x, int y)
    {
        Vector3 origin = Vector3.zero;
        origin.x += (TILE_SIZE * x) + TILE_OFFSET;
        origin.z += (TILE_SIZE * y) + TILE_OFFSET;
        return origin;
    }

    private void DrawChessboard()
    {
        Vector3 widthLine = Vector3.right * 8;
        Vector3 heightLine = Vector3.forward * 8;

        for(int i = 0; i <= 8; i++)
        {
            Vector3 start = Vector3.forward * i;
            Debug.DrawLine(start, start + widthLine);
            for(int j = 0; j <= 8; j++)
            {
                start = Vector3.right * j;
                Debug.DrawLine(start, start + heightLine);
            }
        }

        // Draw the selection
        if(selectionX >= 0 && selectionY >= 0)
        {
            Debug.DrawLine(
                Vector3.forward * selectionY + Vector3.right * selectionX,
                Vector3.forward * (selectionY + 1) + Vector3.right * (selectionX + 1));

            Debug.DrawLine(
               Vector3.forward * (selectionY + 1) + Vector3.right * selectionX,
               Vector3.forward * selectionY + Vector3.right * (selectionX + 1));
        }
    }

    public void PromotionQueen()
    {
        if (isWhiteTurn)
        {
            promotionWhiteQueen = true;
            promotionWhiteKngiht = false;
            //Debug.Log("clickQueenButton");
        }
        else
            promotionBlackQueen = true;
        promotionBlackKngiht = false;
    }

    public void PromotionKnight()
    {
        if (isWhiteTurn)
        {
            promotionWhiteKngiht = true;
            promotionWhiteQueen = false;
            //Debug.Log("clickKnightButton");
        }
        else
            promotionBlackKngiht = true;
            promotionBlackQueen = false;
    }

    private void EndGame()
    {
        if (isWhiteTurn)
            Debug.Log("White team wins");
        else 
            Debug.Log("Black team wins");

        foreach (GameObject go in activeChessman)
            Destroy(go);

        isWhiteTurn = true;
        BoardHighlights.Instance.Hidehighlights();
        SpawnAllChessman();
    }
}
