﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class King : Chessman
{
    public override bool[,] PossibleMove()
    {
        bool[,] r = new bool[8, 8];
        Chessman c;
        int i, j;
 
        //Top Side
        i = CurrentX - 1;
        j = CurrentY + 1;
        if(CurrentY != 7)
        {
            for(int k = 0; k < 3; k++)
            {
                if(i != -1 && j != 8 && i != 8)
                {
                    c = BoardManager.Instance.Chessmans[i, j];

                    if (c == null)
                        r[i, j] = true;
                    //is enemy player
                    else if (isWhite != c.isWhite)
                        r[i, j] = true;
                }
                i++;
            }
        }

        //Down Side
        i = CurrentX - 1;
        j = CurrentY - 1;
        if (CurrentY != 0)
        {
            for (int k = 0; k < 3; k++)
            {
                if (i != -1 && j != -1 && i != 8)
                {
                    c = BoardManager.Instance.Chessmans[i, j];
                    if (c == null)
                        r[i, j] = true;
                    else if (isWhite != c.isWhite)
                        r[i, j] = true;
                }
                i++;
            }
        }

        //White King Middle Left
        i = CurrentX - 1;
        j = CurrentY;
   
        if (CurrentX != 0 && isWhite)
        {
            c = BoardManager.Instance.Chessmans[i, j];
            if (c == null)
            {
                r[i, j] = true;
                if (CurrentX == 4 && CurrentY == 0 && whiteCastlingFlag == false && BoardManager.Instance.Chessmans[1, 0] == null)
                {
                    //Castling
                    Castling(CurrentX - 2, 0, ref r);
                    whiteCastlingFlag = true;
                }
                else if(CurrentX < 4 || CurrentX > 4)
                    whiteCastlingFlag = true;
            }
            else if (isWhite != c.isWhite)
                r[i, j] = true;
        }

        //White King Middle Right
        i = CurrentX + 1;
        j = CurrentY;
 
        if (CurrentX != 7 && isWhite)
        {
            c = BoardManager.Instance.Chessmans[i, j];
            if (c == null)
            {
                r[i, j] = true;
                if (CurrentX == 4 && CurrentY == 0 && whiteCastlingFlag == false)
                {
                    //Castling
                    Castling(CurrentX + 2, 0, ref r);
                    whiteCastlingFlag = true;
                }
                else if (CurrentX < 4 || CurrentX > 4)
                    whiteCastlingFlag = true;
            }  
            else if (isWhite != c.isWhite)
                r[i, j] = true;
        }

        //Black King Middle Left
        i = CurrentX - 1;
        j = CurrentY;

        if (CurrentX != 0 && !isWhite)
        {
            c = BoardManager.Instance.Chessmans[i, j];
            if (c == null)
            {
               
                r[i, j] = true;
                if (CurrentX == 4 && CurrentY == 7 && blackCastlingFlag == false && BoardManager.Instance.Chessmans[1, 7] == null)
                    //Castling
                    Castling(CurrentX - 2, 7, ref r);
                else if (CurrentX < 4 || CurrentX > 4)
                    blackCastlingFlag = true;
            }
            else if (isWhite != c.isWhite)
                r[i, j] = true;
        }

        //Black King Middle Right
        i = CurrentX + 1;
        j = CurrentY;

        if (CurrentX != 7 && !isWhite)
        {
            c = BoardManager.Instance.Chessmans[i, j];
            if (c == null)
            {
                r[i, j] = true;
                if (CurrentX == 4 && CurrentY == 7 && blackCastlingFlag == false)
                    //Castling
                    Castling(CurrentX + 2, 7, ref r);
                else if (CurrentX < 4 || CurrentX > 4)
                    blackCastlingFlag = true;
            }
            else if (isWhite != c.isWhite)
                r[i, j] = true;
        }
        return r;
    }

    //Castling
    public void Castling(int x, int y, ref bool[,] r)
    {
        Chessman c;
        if (x >= 0 && x < 8 && y >= 0 && y < 8)
        {
            c = BoardManager.Instance.Chessmans[x, y];
            if (c == null)
                r[x, y] = true;
            else if (isWhite != c.isWhite)
                r[x, y] = true;
        }
    }
}
